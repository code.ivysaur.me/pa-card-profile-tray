#ifndef QPULSE_H
#define QPULSE_H

#include <QObject>
#include <pulse/pulseaudio.h>

class QPulse : public QObject
{
    Q_OBJECT
public:
    explicit QPulse(QObject *parent = 0);
    ~QPulse();

    pa_context_state_t ConnectionState();

    bool RequestServerInfo();
    bool RequestCardInfo();
    bool SetCardProfile(int32_t card_index, const char* profile_name);

signals:
    void ConnectionStateChanged();
    void GotServerInfo(const pa_server_info& i);
    void GotCardInfoList(const pa_card_info& i, int eol);
    void SetCardProfileOperationCompleted(int success);

public slots:

protected:
    pa_threaded_mainloop* ml;
    pa_context *c;

private:
    static void onConnectionStateChangedSt(pa_context *c, void *userdata);
    static void onGotServerInfoSt(pa_context *c, const pa_server_info *i, void *userdata);
    static void onGotCardInfoListSt(pa_context *c, const pa_card_info *i, int eol, void *userdata);
    static void onSetCardProfileCompleteSt(pa_context *c, int success, void *userdata);

};

#endif // QPULSE_H
