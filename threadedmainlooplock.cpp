#include "threadedmainlooplock.h"

ThreadedMainLoopLock::ThreadedMainLoopLock(pa_threaded_mainloop* ml) :
    ml(ml)
{
    pa_threaded_mainloop_lock(ml);
}

ThreadedMainLoopLock::~ThreadedMainLoopLock() {
    pa_threaded_mainloop_unlock(ml);
}
