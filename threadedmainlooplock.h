#ifndef THREADEDMAINLOOPLOCK_H
#define THREADEDMAINLOOPLOCK_H

#include <pulse/pulseaudio.h>
#include <QObject>

class ThreadedMainLoopLock {
public:
    ThreadedMainLoopLock(pa_threaded_mainloop *ml);
    ~ThreadedMainLoopLock();

private:
    Q_DISABLE_COPY(ThreadedMainLoopLock)

private:
    pa_threaded_mainloop* ml;
};

#endif // THREADEDMAINLOOPLOCK_H
