QT += core gui widgets

TARGET = pa-card-profile-tray
TEMPLATE = app

PKGCONFIG += libpulse
LIBS += -lpulse

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp \
    trayicon.cpp \
    qpulse.cpp \
    threadedmainlooplock.cpp

HEADERS  += \
    trayicon.h \
    qpulse.h \
    threadedmainlooplock.h
