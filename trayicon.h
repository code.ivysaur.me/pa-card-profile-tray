#ifndef TRAYICON_H
#define TRAYICON_H

#include <QObject>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QVector>
#include "qpulse.h"

class TrayIcon : public QSystemTrayIcon
{
public:
    TrayIcon(QObject *parent);
    ~TrayIcon();

    void refreshData();

public slots:
    void on_quitAction_triggered();
    void onRefreshAction_triggered();
    void onGotPulseCardInfo(const pa_card_info& i, int eol);

protected:
    QPulse pulse;
    QMenu* menu;
    QVector<QMenu*> cardMenus;
    QAction* quitAction;
    QAction* refreshAction;
};

#endif // TRAYICON_H
