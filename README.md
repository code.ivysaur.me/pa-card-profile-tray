# pa-card-profile-tray

Tray applet to change audio card profiles in PulseAudio.

## Compile

```
qmake
make
```

## Run at system startup

```
cp pa-card-profile-tray.desktop ~/.config/autostart/
```
