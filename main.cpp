#include <QApplication>
#include "trayicon.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName(QStringLiteral("Set audio card profile..."));

    auto ic = new TrayIcon(&a);
    ic->show();

    return a.exec();
}
