#include "qpulse.h"
#include "threadedmainlooplock.h"

QPulse::QPulse(QObject *parent) :
    QObject(parent),
    ml(pa_threaded_mainloop_new()),
    c(nullptr)
{
    int start_retries = 5;
    int err = -1;
    for(; start_retries --> 0;) {
        err = pa_threaded_mainloop_start(ml);
        if (err == 0) {
            break;
        }
    }

    // Fatal
    if (err != 0) {
        perror("pa_threaded_mainloop_start() failure\n");
        exit(EXIT_FAILURE);
    }

    c = pa_context_new( pa_threaded_mainloop_get_api(ml), "pa-card-profile-tray");
    pa_context_set_state_callback(c, &QPulse::onConnectionStateChangedSt, this);
    pa_context_connect(c, nullptr, PA_CONTEXT_NOFLAGS, nullptr);
}

QPulse::~QPulse()
{
    pa_context_disconnect(c);
    pa_threaded_mainloop_stop(ml);
    pa_threaded_mainloop_free(ml);
}

pa_context_state_t QPulse::ConnectionState()
{
    //ThreadedMainLoopLock lock(ml);

    return pa_context_get_state(c);
}

bool QPulse::RequestServerInfo()
{
    //ThreadedMainLoopLock lock(ml);

    pa_operation* o = pa_context_get_server_info(c, &QPulse::onGotServerInfoSt, this);
    if (! o) {
        return false;
    }

    pa_operation_unref(o);
    return true;
}

bool QPulse::RequestCardInfo()
{
    //ThreadedMainLoopLock lock(ml);

    pa_operation* o = pa_context_get_card_info_list(c, &QPulse::onGotCardInfoListSt, this);
    if (! o) {
        return false;
    }

    pa_operation_unref(o);
    return true;
}

bool QPulse::SetCardProfile(int32_t card_index, const char* profile_name)
{
    pa_operation* o = pa_context_set_card_profile_by_index(c, card_index, profile_name, &QPulse::onSetCardProfileCompleteSt, this);
    if (! o) {
        return false;
    }

    pa_operation_unref(o);
    return true;
}

void QPulse::onGotCardInfoListSt(pa_context*, const pa_card_info *i, int eol, void *userdata)
{
    QPulse* qp = static_cast<QPulse*>(userdata);
    //ThreadedMainLoopLock lock(qp->ml);

    emit qp->GotCardInfoList(*i, eol);
}

void QPulse::onGotServerInfoSt(pa_context*, const pa_server_info *i, void *userdata)
{
    QPulse* qp = static_cast<QPulse*>(userdata);
    //ThreadedMainLoopLock lock(qp->ml);

    emit qp->GotServerInfo(*i);
}

void QPulse::onConnectionStateChangedSt(pa_context*, void *userdata)
{
    QPulse* qp = static_cast<QPulse*>(userdata);
    //ThreadedMainLoopLock lock(qp->ml);

    emit qp->ConnectionStateChanged();
}

void QPulse::onSetCardProfileCompleteSt(pa_context*, int success, void *userdata)
{
    QPulse* qp = static_cast<QPulse*>(userdata);
    //ThreadedMainLoopLock lock(qp->ml);

    emit qp->SetCardProfileOperationCompleted(success);
}
